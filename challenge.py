# -*- coding: utf-8 -*-
"""
Created on Wed May 24 05:42:18 2023

@author: Daurade Magneres R
"""

import psycopg2
#from challenge1.dbt import load_dw
import os


# Establecer la conexión con la base de datos
conn = psycopg2.connect( 
    host="localhost",
    port="5432",
    dbname="dw_flights",
    user="postgres",
    password="Password1234**"
)

cursor  = conn.cursor()

# Ejecutar las queries para crear las tablas
# archivo_sql = os.path.join("C:\\Users\\ruthd\\Documents\\data-engineer-coding-interview\\challenge1", "archivo.sql")
# with open(archivo_sql, "r") as file:
#     queries = file.read()
# cursor.execute(queries)

cursor.execute('''CREATE TABLE airlines(
    carrier VARCHAR(2),
    name VARCHAR NOT NULL,
    CONSTRAINT p_airline PRIMARY KEY (carrier)
);''')
cursor.execute('''SELECT * FROM airlines;''')

for row in cursor:
     print(row)
     
# Confirmar los cambios y cerrar la conexión
# conn.commit()
# conn.close()
